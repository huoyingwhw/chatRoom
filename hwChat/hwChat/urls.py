"""hwChat URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from chat import views
from utils.verification_img import get_verification_img

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r"^$",views.Login.as_view(),name="root_login"),
    url(r"^login/$",views.Login.as_view(),name="login"),
    url(r"^register/$",views.Register.as_view(),name="register"),
    url(r"^logout/$",views.Logout.as_view(),name="logout"),
    url(r"^talk_room/$",views.TalkRoom.as_view(),name="talk_room"),
    url(r'^update_pwd/$',views.Update_pwd.as_view(),name="update_pwd"),
    # 获取图片验证码的逻辑
    url(r'^get_verification_img/$',get_verification_img,name="get_verification_img"),
    # 下面是聊天室的逻辑
    url(r'^accept_message/$',views.accept_message,name="accept_message"),
    url(r'^accept_single_message/$',views.accept_single_message,name="accept_single_message"),
    url(r'^send_message/$',views.send_message,name="send_message"),
    url(r'^send_single_message/$',views.send_single_message,name="send_single_message"),
    url(r'^send_single_message/length$',views.send_single_message_len,name="send_single_message_len"),
    url(r'^send_user/$',views.send_user,name="send_user"),
    url(r'^close_browser/$',views.close_browser,name="close_browser"),

]
