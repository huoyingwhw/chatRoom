
# 从项目配置目录，导入setting文件
from hwChat import settings
from django.shortcuts import render,HttpResponse,redirect
from django.urls import reverse

# 第三方模块pillow
from PIL import Image,ImageDraw,ImageFont
# python内置模块
from io import BytesIO
import os
import random

# 获得随机RGB颜色
def get_random_color():
    return (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))

# 验证码取值列表
random_list = [str(i) for i in range(0,10)]  # 数字
random_list += [chr(i) for i in range(97,123)] # 小写字母
random_list += [chr(i) for i in range(65,91)] # 大写字母

def get_verification_img(request):
    # 验证码图片的大小
    width = 200
    height = 34

    # Image.new(mode="图片颜色模式",size=图片大小,color=图片的背景色)
    img_obj = Image.new(mode='RGB', size=(width,height),color=get_random_color()) #图片对象
    draw_obj = ImageDraw.Draw(img_obj)  #通过图片对象生成一个画笔对象
    font_path = os.path.join(settings.BASE_DIR,'staticfiles/font/COOPBL.TTF') #获取字体,注意有些字体文件不能识别数字，所以如果数字显示不出来，就换个字体
    font_obj = ImageFont.truetype(font_path,size=18) #创建字体对象
    # verification_code = ''.join(random.choices(random_list,k=6))  # 6位的随机字符串验证码
    verification_code = ''.join(random.choices(["1","2","3","4","5","6","7"],k=5))  # 5位的随机字符串验证码

    print("%s用户的验证码：%s"%(request.user,verification_code))
    # text(xy=起始的位置(使文本内容居中)，text=内容，fill=文本颜色，font=使用的字体)
    draw_obj.text(xy=(60,9),text=verification_code,fill=get_random_color(),font=font_obj) #通过画笔对象，添加文字

    # 添加噪线
    for i in range(3):
        # 一个坐标表示一个点，两个点就可以连成一条线
        x1 = random.randint(0, width//20)
        x2 = random.randint(0, width//20)
        y1 = random.randint(0, height//20)
        y2 = random.randint(0, height//20)
        # line(xy=两点的位置,fill=线的颜色)
        draw_obj.line((x1, y1, x2, y2), fill=get_random_color(),width=3)

    # # 添加噪点或躁弧
    for i in range(10):
        # 噪点
        # draw_obj.point([random.randint(0, width), random.randint(0, height)], fill=get_random_color())
        # 噪弧
        x = random.randint(0, width)
        y = random.randint(0, height)
        # arc(xy=两点的位置,star=弧开始的度数,end=弧画到结束的度数,fill=弧线的颜色)
        draw_obj.arc((x, y, x + 1, y + 1), start=random.randint(0,3), end=random.randint(0,3), fill=get_random_color())  # x, y是弧线的起始点位置，x + 4, y + 4是弧线的结束点位置

    f = BytesIO()  #操作内存的把手
    img_obj.save(f,'png')  # 以png格式保存至内存？
    data = f.getvalue()  # 获取图片数据
    request.session['verification_code'] = verification_code
    return HttpResponse(data)  # 发送图片
