from django.db import models
from django.contrib.auth.models import AbstractUser

class UserInfo(AbstractUser):
    # 扩展字段
    telephone = models.CharField(max_length=33,null=True)

    def __str__(self):
        return self.username