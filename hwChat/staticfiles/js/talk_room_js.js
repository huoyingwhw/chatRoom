function show_my_message(content) {
    $('#talk-content').append(`
        <div class="message-sum clearfix">
            <div class="user text-right">${now_user} :</div>
            <span class="message pull-right">${content}</span>
        </div>
    `);
}

function show_other_message(dic) {
    $('#talk-content').append(`
        <div class="message-sum">
            <div class="user">${dic["user"]} :</div>
            <span class="message">${dic["message"]}</span>
        </div>
    `);
}

//清空输入框的内容
function clear_my_input() {
    $('#input-content').val("");
}

// 聊天框让其往下翻动
var talk_obj = $('#talk-content')[0];

// 聊天框信息超出默认是不动，需要手动设置一下距离顶部的位置
function pull_down() {
    talk_obj.scrollTop = talk_obj.scrollHeight
}

//单人聊天框让往下翻动
var single_talk_obj = $('#single-person-talk-content')[0];

function single_pull_down() {
    single_talk_obj.scrollTop = single_talk_obj.scrollHeight
}


// 清空所有人的聊天列表
function clear_talk_list() {
    $('#show_user').children().remove();
}


// 主聊天窗口移动

move_main_talk_window = false;  //移动聊天窗口的标志位
var befor_x, befor_y;  // 移动窗口之前的左、上像素值
var x_position, y_position;  // 移动窗口之前的用户点击标签的位置
$('#main-talk-title').mousedown(
    function (e) {
        if (!move_main_talk_window) {  // 按下后只执行一次
            // console.log(e);
            move_main_talk_window = true;
            // console.log("按下");
            befor_x = $('#main-talk-window').offset()["left"];
            befor_y = $('#main-talk-window').offset()["top"];
            x_position = e.clientX;
            y_position = e.clientY;
            // console.log("点击位置：",x_position,y_position);
        }
    }
);

$('#main-talk-title').mouseup(
    function () {
        move_main_talk_window = false;
        // console.log("松开");
    }
);

var talk_window_width = 700; // 聊天窗口的宽度
var talk_window_height = 500;  // 聊天窗口的高度
$(document).mousemove(  //鼠标在文档上的移动事件
    function (e) {
        if (move_main_talk_window) {  // 当用户点击了聊天窗口要移动
            // 未移动前的x轴像素+ 鼠标移动的像素值(当前鼠标x轴位置-点击窗口的x位置)
            var temp_x = befor_x + (e.pageX - x_position);
            var temp_y = befor_y + (e.pageY - y_position);
            // 判断聊天窗口是否越界,文档高度-100是因为windows的任务栏的缘故
            if (temp_x > 0 && temp_x < screen.availWidth - talk_window_width && temp_y > 0 && temp_y < (screen.availHeight - talk_window_height - 100)) {
                $('#main-talk-window').offset({left: temp_x, top: temp_y});
            }
        }
    }
);


// 单人聊天窗口移动

move_single_person_talk_window = false;  //移动聊天窗口的标志位
var s_befor_x, s_befor_y;  // 移动窗口之前的左、上像素值
var s_x_position, s_y_position;  // 移动窗口之前的用户点击标签的位置
$('#single-person-talk-title').mousedown(
    function (e) {
        if (!move_single_person_talk_window) {  // 按下后只执行一次
            move_single_person_talk_window = true;
            s_befor_x = $('#single-person-talk-window').offset()["left"];
            s_befor_y = $('#single-person-talk-window').offset()["top"];
            s_x_position = e.clientX;
            s_y_position = e.clientY;
        }
    }
);

$('#single-person-talk-window').mouseup(
    function () {
        move_single_person_talk_window = false;
    }
);

var single_person_talk_window_width = 500; // 聊天窗口的宽度
var single_person_talk_window_height = 500;  // 聊天窗口的高度
$(document).mousemove(  //鼠标在文档上的移动事件
    function (e) {
        if (move_single_person_talk_window) {
            var temp_x = s_befor_x + (e.pageX - s_x_position);
            var temp_y = s_befor_y + (e.pageY - s_y_position);
            if (temp_x > 0 && temp_x < screen.availWidth - single_person_talk_window_width && temp_y > 0 && temp_y < (screen.availHeight - single_person_talk_window_height - 100)) {
                $('#single-person-talk-window').offset({left: temp_x, top: temp_y});
            }
        }
    }
);

// 清空单人聊天输入框内容
function clear_single_person_input() {
    $('#single-person-input-content').val("");
}

function clear_single_content() {
    single_message_num = 0;
    $('#single-person-talk-content').text("");
}

var single_task;

// 点击单人聊天框关闭，添加隐藏并清空输入框内容
$('#single-person-talk-close').click(
    function () {
        open_single_window_flag = false;

        // 清除单人聊天的计时器
        clearInterval(single_task);
        // 清空输入框内容
        clear_single_person_input();
        // 清空聊天内容
        clear_single_content();


        $('#single-person-talk-window').addClass("hidden");
    }
);

// 点击聊天列表显示单人聊天框
$('#show_user').on('click', '.user_show', function (e) {
    if (!open_single_window_flag) {
        // 使用户点击开一个单人窗口之后，必须关闭才能切换
        open_single_window_flag = true;

        talk_username = $(this).text();
        $('#single-person-talk-name').text(talk_username);

        // 开启定时任务，实时接收点击开的用户信息
        single_message_send_task();
        single_task = setInterval(single_message_send_task, 1000);


        // console.log(this);
        $('#single-person-talk-window').removeClass("hidden");
    }
});

$('#talk_with_me').on('click', '.user_show', function (e) {
    if (!open_single_window_flag) {
        // 使用户点击开一个单人窗口之后，必须关闭才能切换
        open_single_window_flag = true;

        talk_username = $(this).find('.talk_name').text();
        $('#single-person-talk-name').text(talk_username);

        // 开启定时任务，实时接收点击开的用户信息
        single_message_send_task();
        single_task = setInterval(single_message_send_task, 1000);

        // console.log(this);
        $('#single-person-talk-window').removeClass("hidden");
    }
});

function show_single_my_message(content) {
    $('#single-person-talk-content').append(`
        <div class="message-sum clearfix">
            <div class="user text-right">${now_user} :</div>
            <span class="message pull-right">${content}</span>
        </div>
    `);
}

// 单人框 展示其他用户的聊天内容
function show_single_other_message(dic) {
    $('#single-person-talk-content').append(`
        <div class="message-sum">
            <div class="user">${dic["user"]} :</div>
            <span class="message">${dic["message"]}</span>
        </div>
    `);
}

function clear_talk_with_me() {
    $('#talk_with_me').children().remove();
}



