# -*- coding:utf-8 -*-
import re

from django.utils.deprecation import MiddlewareMixin
from django.shortcuts import redirect
from django.urls import reverse

WHITE_LIST = ["/admin/.*","/login/","/","/register/","/logout/","/get_verification_img/"]
##### 写在process_request方法中！
# print(request.path_info)
#         # 白名单放行
#         for i in WHITE_LIST:
#             if re.search(request.path_info,i):
#                 return


class AuthMiddleware(MiddlewareMixin):

    def process_request(self,request):
        pass
        # white_list = [reverse('login'), reverse('register'), reverse('get_verification_img')]
        # # 如果用户请求的不是登陆页面，就进行下一次判断
        # if request.path not in white_list:
        #     path = request.path
        #     # 如果用户认证失败跳转登陆页面
        #     if not request.user.is_authenticated():
        #         return redirect(reverse('login') + "?next=" + path)

